<?php
  include ("all-of-those-images/interf/jpgraph/src/jpgraph.php");
  include ("all-of-those-images/interf/jpgraph/src/jpgraph_line.php");
  
  include 'dbc.php';
  $conn = mysqli_connect($host, $user, $pass, $db);
  $no=$_GET['no'];
  $gra=$_GET['gra'];
  $d1=$_GET['d1'];
  $d2=$_GET['d2'];
  switch($no)
  {
    case 1:
      switch($gra)
      {
        case 1:
          $sql = "select Rday,VMp from historico ";
          $some='VMp';
          $thone='#820000';
          $filler='#b52f2f';
          $tt='Maquinas pendientes';
          break;
        case 2:
          $sql = "select Rday,VMe from historico ";
          $some='VMe';
          $thone='#32a852';
          $filler='#7cf79d';
          $tt='Maquinas entragadas';
          break;
        case 3:
          $sql = "select Rday,VMt from historico ";
          $some='VMt';
          $thone='#2f41b5';
          $filler='#2f91b5';
          $tt='Total de maquinas';
          break;
      }
      break;
    case 2:
      switch($gra)
      {
        case 1:
          $sql = "select Rday,VCPUp from historico ";
          $some='VCPUp';
          $thone='#820000';
          $filler='#b52f2f';
          $tt='vCPU pendientes';
        break;
        case 2:
          $sql = "select Rday,VCPUe from historico ";
          $some='VCPUe';
          $thone='#32a852';
          $filler='#7cf79d';
          $tt='vCPU entregados';
        break;
        case 3:
          $sql = "select Rday,VCPUt from historico ";
          $some='VCPUt';
          $thone='#2f41b5';
          $filler='#2f91b5';
          $tt='vCPU totales';
        break;
      }
    break;
    case 3:
      switch($gra)
      {
        case 1:
          $sql = "select Rday,RAMp from historico ";
          $some='RAMp';
          $thone='#820000';
          $filler='#b52f2f';
          $tt='RAM pendiente';
        break;
        case 2:
          $sql = "select Rday,RAMe from historico ";
          $some='RAMe';
          $thone='#32a852';
          $filler='#7cf79d';
          $tt='RAM entregada';
        break;
        case 3:
          $sql = "select Rday,RAMt from historico ";
          $some='RAMt';
          $thone='#2f41b5';
          $filler='#2f91b5';
          $tt='RAM total';
        break;
      }
    break;
    case 4:
      switch($gra)
      {
        case 1:
          $sql = "select Rday,DDp from historico ";
          $some='DDp';
          $thone='#820000';
          $filler='#b52f2f';
          $tt='Disco duro pendiente';
        break;
        case 2:
          $sql = "select Rday,DDe from historico ";
          $some='DDe';
          $thone='#32a852';
          $filler='#7cf79d';
          $tt='Disco duro entregado';
        break;
        case 3:
          $sql = "select Rday,DDt from historico ";
          $some='DDt';
          $thone='#2f41b5';
          $filler='#2f91b5';
          $tt='Disco duro total';
        break;
      }
    break;
    case 5:
      switch($gra)
      {
        case 1:
          $sql = "select Rday,DCp from historico ";
          $some='DCp';
          $thone='#820000';
          $filler='#b52f2f';
          $tt='Disco compartido pendiente';
        break;
        case 2:
          $sql = "select Rday,DCe from historico ";
          $some='DCe';
          $thone='#32a852';
          $filler='#7cf79d';
          $tt='Disco compartido entregado';
        break;
        case 3:
          $sql = "select Rday,DCt from historico ";
          $some='DCt';
          $thone='#2f41b5';
          $filler='#2f91b5';
          $tt='Disco compartido total';
        break;
      }
    break;
    default:
      header('Location: http://historico-ex379816-project-dev.apps.paas.telcelcloud.dt/');
    break;
  }
  if($d1!="")
  {
    if($d2!=""&&$d1<$d2)
    {
      $temp=explode("/",$d1);
      $temp2=$temp[0];
      $temp[0]=$temp[2];
      $temp[2]=$temp2;
      $d1=implode("-",$temp);
      $temp=explode("-",$d1);
      for($k=0;$k<3;$k++)
        $temp[$k]=$temp[$k+1];
      unset($temp[3]);
      $d1=implode("-",$temp);  
      $temp=explode("/",$d2);
      $temp2=$temp[0];
      $temp[0]=$temp[2];
      $temp[2]=$temp2;
      $d2=implode("-",$temp);
      $temp=explode("-",$d2);
      for($k=0;$k<3;$k++)
        $temp[$k]=$temp[$k+1];
      unset($temp[3]);
      $d2=implode("-",$temp);
      $sql .= "where Rday>='".$d1."' and Rday<='".$d2."' order by Rday ";  
    }
    else
    {
      $temp=explode("/",$d1);
      $temp2=$temp[0];
      $temp[0]=$temp[2];
      $temp[2]=$temp2;
      $d1=implode("-",$temp);
      $temp=explode("-",$d1);
      for($k=0;$k<3;$k++)
        $temp[$k]=$temp[$k+1];
      unset($temp[3]);
      $d1=implode("-",$temp);  
      $sql .= "where Rday>='".$d1."' order by Rday";
    }
  }
  else
    $sql .= "order by Rday limit 10";
  $re = mysqli_query($conn,$sql);
  $i=0;
  while ($row=mysqli_fetch_array($re))
  {
    $other[$i]=$row['Rday'];
    $ydata[$i]=$row[$some];
    $i++;
  }
  mysqli_close($conn);
  $graph = new Graph(500,280);
  $graph->SetBox(); 
  $graph->SetScale("textlin");
  $graph->img->SetAntiAliasing();
  $graph->xgrid->Show();
  $graph->ygrid->SetFill(true,'#DDDDDD@0.5','#BBBBBB@0.5');
  $graph->ygrid->SetLineStyle('dashed');
  $graph->ygrid->SetColor('gray');  
  $graph->xgrid->SetLineStyle('dashed');
  $graph->xgrid->SetColor('gray');
  $graph->SetBox(false);
  $lineofy=new LinePlot($ydata);
  $lineofy->SetWeight(1);
  //$lineofy->SetLegend($some);
  $graph->img->SetMargin(60,30,30,60);
  $graph->title->Set($tt);
  $graph->xaxis->title->Set($i." registros en el periodo definido");
  $graph->xaxis->SetTickLabels($other);
  //$graph->xaxis->SetFont(FF_ARIAL,FS_NORMAL,6);
  $graph->SetShadow();
  $graph->yaxis->HideLine(false);
  $graph->yaxis->HideTicks(false,false);
  $graph->yaxis->HideZeroLabel();
  $graph->Add($lineofy);
  $lineofy->SetFillColor($filler.'@0.8');
  $lineofy->SetColor($thone);
  $lineofy->value->SetFormat('%d');
  $lineofy->value->Show();
  $graph->Stroke();
?>