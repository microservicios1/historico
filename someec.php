<?php
  include 'dbc.php';
  $conn = mysqli_connect($host, $user, $pass, $db);
  if( $conn )
  {
    $fl=0;
    mysqli_begin_transaction($conn);
    $sql="select SVCPU,SRAM,SSto,DVCPU,DRAM,DSto from maquinas";
    $re = mysqli_query($conn,$sql);
    $r=mysqli_affected_rows($conn);
    if($r>0)
    {
      $pvm=0;
      $evm=0;
      $vm=array('ecpu' =>0,'emem' =>0,'esd'=>0,'scpu' =>0,'smem' =>0,'ssd'=>0,'SDQ' =>0,'EDQ' =>0);
      while($row0 = mysqli_fetch_array($re))
      {
        $vm['ecpu'] += $row0['DVCPU'];
        $vm['emem'] += $row0['DRAM'];
        $vm['esd'] += $row0['DSto'];
        $vm['scpu'] += $row0['SVCPU'];
        $vm['smem'] += $row0['SRAM'];
        $vm['ssd'] += $row0['SSto'];
        if(($row0['DVCPU']>=$row0['SVCPU'])&&($row0['DRAM']>=$row0['SRAM'])&&($row0['DSto']>=$row0['SSto']))
          $evm++;
        else
          $pvm++;
      }
      $sql="select SDQ,EDQ from ShaDQ";
      $re = mysqli_query($conn,$sql);
      while($row0 = mysqli_fetch_array($re))
      {
        $vm['SDQ'] += $row0['SDQ'];
        $vm['EDQ'] += $row0['EDQ'];
      }
      $time= new DateTime();
      $time=$time->format('Y-m-d');
      $sql="delete from historico where Rday='".$time."'";
      mysqli_query($conn,$sql);
      $sql = "insert into historico values ('".$time."',".$pvm.",".$evm.",";
      $some = $pvm+$evm;
      $sql .= $some.",";
      $some = $vm['SDQ']-$vm['EDQ'];
      $sql .= $some.",".$vm['EDQ'].",".$vm['SDQ'].",";
      $some = $vm['scpu']-$vm['ecpu'];
      $sql .= $some.",".$vm['ecpu'].",".$vm['scpu'].",";
      $some = $vm['smem']-$vm['emem'];
      $sql .= $some.",".$vm['emem'].",".$vm['smem'].",";
      $some = $vm['ssd']-$vm['esd'];
      $sql .= $some.",".$vm['esd'].",".$vm['ssd'].")";
      mysqli_query($conn,$sql);
      $r=mysqli_affected_rows($conn);
      if($r==1)
        mysqli_commit($conn);
      else
        mysqli_rollback($conn);
    }
  }
  mysqli_close($conn);
?>